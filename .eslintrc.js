const path = require("path");

module.exports = {
    globals: {
        config: false,
        dconfig: false,
        t: false,
    },
    settings: {
        "import/resolver": {
            webpack: {
                config: path.resolve(__dirname, "config/webpack.config.js"),
                "config-index": 1,
            },
        },
    },
    extends: ["../.eslintrc.json", "@telokys/eslint-config-react-typescript"],
    plugins: ["babel"],
    rules: {
        "babel/no-unused-expressions": "error",
        "jsx-a11y/anchor-is-valid": [
            "error",
            {
                components: ["Link"],
                specialLink: ["to", "hrefLeft", "hrefRight"],
                aspects: ["noHref", "invalidHref", "preferButton"],
            },
        ],
        "jsx-a11y/alt-text": "off",
        "@typescript-eslint/class-name-casing": "off",
        "@typescript-eslint/no-empty-function": "off",
        "no-empty-function": "off",
        "no-unused-expressions": "off",
        "max-classes-per-file": "off",
        "no-unused-vars": "off",
        "import/no-cycle": "off",
        "babel/no-unused-expressions": "off",
        "react/state-in-constructor": ["error", "never"],
        "react/jsx-props-no-spreading": "off",
        "no-console": ["error", { allow: ["warn", "error"] }],
        "default-case": "off",
        "react/static-property-placement": [
            "error",
            "static public field",
            {
                propTypes: "property assignment",
            },
        ],
    },
    overrides: [
        {
            files: require("./package.json").jest.testMatch,
            env: {
                jest: true,
                browser: true,
            },
            rules: {
                "no-restricted-modules": "off",
                "no-restricted-imports": "off",
            },
        },
        {
            files: "./config/production.js",
            env: {
                node: true,
            },
        },
    ],
};
