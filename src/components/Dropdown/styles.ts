import { makeStyles } from "@material-ui/core";

export default makeStyles({
    select: {
        cursor: "pointer",
    },
});
