import clsx from "clsx";
import React, { FC, memo } from "react";
import useStyles from "./styles";

type ValueType = string | number | readonly string[] | undefined;

interface DropdownProps {
    options: Array<{
        value: ValueType;
        label: string | number;
    }>;
    value?: ValueType;
    onChange: (value: ValueType) => void;
    className?: string;
    selectClassname?: string;
}

const Dropdown: FC<DropdownProps> = ({ options, onChange, className, selectClassname, value }) => {
    const classes = useStyles();

    return (
        <span className={clsx("grepo_input", className)}>
            <span className="left">
                <span className="right">
                    <select
                        value={value}
                        className={clsx(selectClassname, classes.select)}
                        onChange={(e) => onChange(e.currentTarget.value)}
                    >
                        {options.map((option) => (
                            <option key={`${option.value}-${option.label}`} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </select>
                </span>
            </span>
        </span>
    );
};

export default memo(Dropdown);
