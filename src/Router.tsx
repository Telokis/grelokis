import React from "react";
import { Switch, Route, Redirect } from "react-router";

import BuildingsPage from "$pages/BuildingsPage";

export default () => (
    <Switch>
        <Route exact path="/buildings" component={BuildingsPage} />
        <Route render={() => <Redirect to="/buildings" />} />
    </Switch>
);
