import { normalBuildings } from "$data/buildings";
import React, { FC, useMemo } from "react";
import SingleBuilding from "./SingleBuilding";
import clsx from "clsx";
import useStyles from "./styles";
import { DelimitedNumericArrayParam, useQueryParam, withDefault } from "use-query-params";
import { Grid } from "@material-ui/core";
import SpecialSection from "./SpecialSection";

const BuildingsPage: FC = () => {
    const classes = useStyles();
    const normalBuildingsValues = useMemo(() => Object.values(normalBuildings), []);
    const [levels, setLevels] = useQueryParam(
        "b",
        withDefault(DelimitedNumericArrayParam, [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
    );
    const [specials, setSpecials] = useQueryParam(
        "s",
        withDefault(DelimitedNumericArrayParam, [9, 9]),
    );
    const levelUpdaters = normalBuildingsValues.map((_, index) => (lvl: number) => {
        const newLevels = [...levels];

        newLevels[index] = lvl;

        setLevels(newLevels, "pushIn");
    });
    const totalPoints = useMemo(
        () =>
            normalBuildingsValues.reduce(
                (sum, building, index) => sum + (building.levels[levels[index]!]?.points || 0),
                33 + (specials[0]! < 4 ? 500 : 0) + (specials[1]! < 4 ? 500 : 0),
            ),
        [levels, specials],
    );
    const totalPop = useMemo(
        () =>
            normalBuildingsValues.reduce(
                (sum, building, index) => sum + (building.levels[levels[index]!]?.pop || 0),
                (specials[0]! < 4 ? 60 : 0) + (specials[1]! < 4 ? 60 : 0),
            ),
        [levels, specials],
    );

    return (
        <div className="gpwindow_content">
            <div className="main_window_background" />

            <div className={clsx("gpwindow_content", classes.window)}>
                <Grid container justify="space-evenly">
                    <Grid item xs={12}>
                        <div>Total de points: {totalPoints} Pts.</div>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={clsx(false && "resource_population_icon res_icon")}>
                            Total de population utilisée: {totalPop}
                        </div>
                    </Grid>
                </Grid>

                <div
                    id="techtree"
                    className={clsx("build_cost_reduction_enabled", classes.buildingArea)}
                >
                    <div id="main_arrow_left" />
                    <div id="main_arrow_right" />

                    <div id="buildings" className="build_cost_reduction_enabled">
                        {normalBuildingsValues.map((building, index) => (
                            <SingleBuilding
                                key={building.id}
                                level={levels[index]!}
                                building={building}
                                onLevelChanged={levelUpdaters[index]}
                            />
                        ))}

                        <SpecialSection
                            onBuildingChanged={(val) => setSpecials([val, specials[1]])}
                            specialIndex={0}
                            activeIndex={specials[0]!}
                        />
                        <SpecialSection
                            onBuildingChanged={(val) => setSpecials([specials[0], val])}
                            specialIndex={1}
                            activeIndex={specials[1]!}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BuildingsPage;
