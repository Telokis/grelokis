import { specialBuildings } from "$data/buildings";
import clsx from "clsx";
import React, { FC, useMemo, memo } from "react";
import useStyles from "./styles";

interface SpecialSectionProps {
    specialIndex: 0 | 1;
    activeIndex: number;
    onBuildingChanged: (index: number) => void;
}

const SpecialSection: FC<SpecialSectionProps> = ({
    specialIndex,
    activeIndex,
    onBuildingChanged,
}: SpecialSectionProps) => {
    const classes = useStyles();
    const indexStart = useMemo(() => (specialIndex === 0 ? 0 : 4), [specialIndex]);
    const values = useMemo(
        () =>
            Object.values(specialBuildings).filter((_, i) => i >= indexStart && i < indexStart + 4),
        [],
    );

    return (
        <div id={`special_group_${specialIndex + 1}`}>
            <div className="building_arrow" />
            <div className="building_special">
                {values.map((building, index) => (
                    <div
                        onClick={() => onBuildingChanged(index === activeIndex ? 9 : index)}
                        key={building.id}
                        title={building.name}
                        id={`special_building_${building.id}`}
                        style={{
                            backgroundImage: `url(${process.env.PUBLIC_URL}/${building.image})`,
                            letterSpacing: "-1px",
                        }}
                        className={clsx(
                            "image special_build",
                            index !== activeIndex && classes.inactive,
                        )}
                    />
                ))}
            </div>
        </div>
    );
};

export default memo(SpecialSection);
