import { makeStyles } from "@material-ui/core";

export default makeStyles({
    window: {
        width: "781px",
        height: "405px",
        overflowY: "hidden",
        margin: "auto",
        marginTop: "0",
        background: "unset !important",
    },
    buildingArea: {
        position: "absolute",
        top: "32px",
    },
    dropdown: {
        position: "absolute",
        top: "38px",
        left: "0px",
    },
    dropdownSelect: {
        outline: "0",
        width: "40px",
        paddingTop: "2px !important",
    },
    buildingPop: {
        position: "relative !important" as "relative",
        display: "inline-block",
        textAlign: "right",
        paddingLeft: "0 !important",
        paddingRight: "25px !important",
        backgroundPositionX: "31px !important",
        backgroundPositionY: "-515px !important",
    },
    buildingPoints: {
        display: "inline-block",
        paddingLeft: "5px",
    },
    infosWrapper: {
        textAlign: "left",
        padding: "0 5px",
    },
    level: {
        bottom: "3px !important",
        "&.white": {
            bottom: "4px !important",
        },
    },
    inactive: {
        filter: "grayscale(1) blur(1px)",
    },
});
