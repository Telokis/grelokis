import Dropdown from "$components/Dropdown";
import { Building } from "$data/buildings";
import { Grid } from "@material-ui/core";
import clsx from "clsx";
import React, { FC, useMemo, memo } from "react";
import useStyles from "./styles";

interface SingleBuildingProps {
    level: number;
    building: Building;
    onLevelChanged: (level: number) => void;
}

const SingleBuilding: FC<SingleBuildingProps> = ({
    level,
    building,
    onLevelChanged,
}: SingleBuildingProps) => {
    const classes = useStyles();
    const options = useMemo(
        () =>
            building.levels.map((level) => ({
                value: level.level,
                label: level.level,
            })),
        [building],
    );

    return (
        <div id={`building_main_${building.id}`}>
            <div className="building_arrow" />
            <div className="building">
                <div
                    style={{
                        backgroundImage: `url(${process.env.PUBLIC_URL}/${building.image})`,
                        letterSpacing: "-1px",
                    }}
                    className="image bold"
                >
                    <span className={clsx("level", classes.level)}>{level}</span>
                    <span className={clsx("level white", classes.level)}>{level}</span>
                </div>

                <Dropdown
                    value={level}
                    className={classes.dropdown}
                    selectClassname={classes.dropdownSelect}
                    options={options}
                    onChange={onLevelChanged}
                />

                <div className="button_build build build_up small bold">
                    <Grid container justify="space-evenly" spacing={3}>
                        <Grid item xs={6}>
                            <div className={classes.buildingPoints}>
                                {building.levels[level]?.points || 0} Pts.
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div
                                className={clsx(
                                    "resource_population_icon res_icon",
                                    classes.buildingPop,
                                )}
                            >
                                {building.levels[level]?.pop || 0}
                            </div>
                        </Grid>
                    </Grid>
                </div>

                <div className="name small bold" style={{ width: "129px" }}>
                    <a>{building.name}</a>
                </div>
            </div>
        </div>
    );
};

export default memo(SingleBuilding);
