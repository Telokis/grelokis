const env = process.env.BABEL_ENV || process.env.NODE_ENV || "development";

const allPlugins = [
    "@babel/plugin-transform-modules-commonjs",
    ["@babel/plugin-transform-for-of", { assumeArray: true }],
    ["@babel/plugin-proposal-decorators", { legacy: true }],
    ["@babel/plugin-proposal-class-properties", { loose: true }],
    "@babel/plugin-proposal-numeric-separator",
    "@babel/plugin-proposal-optional-chaining",
    "@babel/plugin-proposal-nullish-coalescing-operator",
];

module.exports = {
    ignore: ["**/node_modules"],
    babelrcRoots: ["."],
    overrides: [
        {
            test: /\.ts$/,
            plugins: [["@babel/plugin-transform-typescript"], ...allPlugins],
        },
        {
            test: /\.tsx$/,
            plugins: [
                ["@babel/plugin-transform-typescript", { isTSX: true }],
                ...allPlugins,
            ],
        },
        {
            test: /\.jsx?$/,
            plugins: allPlugins,
        },
    ],
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    browsers: [
                        "last 10 versions",
                        "not ie <= 11",
                        "not dead",
                        "not < 0.5%",
                    ],
                },
                exclude: ["transform-for-of", "proposal-private-methods"],
                useBuiltIns: "usage",
                corejs: 3,
            },
        ],
        "@babel/preset-react",
    ],
    plugins: [
        [
            "transform-imports",
            {
                "lodash-es": {
                    transform: "lodash-es/${member}",
                    preventFullImport: true,
                },
                lodash: {
                    transform: "lodash-es/${member}",
                    preventFullImport: true,
                },
                "@material-ui/core": {
                    transform: (name) => {
                        if (
                            [
                                "createStyles",
                                "withStyles",
                                "makeStyles",
                            ].includes(name)
                        ) {
                            return `@material-ui/core/esm/styles/${name}`;
                        }

                        return `@material-ui/core/esm/${name}`;
                    },
                    preventFullImport: true,
                },
            },
        ],
    ],
};

if (env === "debug") {
    module.exports.retainLines = true;
    module.exports.sourceMaps = "inline";
}
